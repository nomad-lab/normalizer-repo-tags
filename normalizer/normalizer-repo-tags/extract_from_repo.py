# Copyright 2017-2018 Jungho Shin, Fawzi Mohamed
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os, sys, json, zipfile
base_path= sys.exec_prefix + '/python'
sys.path.insert(0, base_path)
from nomadrepo.core.api import API

def extract_metadata(dic_in, path_in):
    main_file_uri = dic_in['main_file_uri'][0].replace('nmd://', '')
    main_file_uri_list = main_file_uri.split('/')
    gid = main_file_uri_list[0]
    zip_path = os.path.join(path_in, gid[0:3], gid+'.zip')
    main_file_path = "/".join([gid]+main_file_uri_list[1:])
    with zipfile.ZipFile(zip_path, 'r') as zip_f: main_file_size = zip_f.getinfo(main_file_path).file_size

    work = API()

    for calc, error in work._parse(dic_in, "normalizerRepo"):
        calc, error = work.classify(calc)
        calc.info['oadate'] = None
        calc.info['checksum'] = calc.get_checksum(filesize=main_file_size)
        json.dump(calc.info, sys.__stdout__)
        pass

def main():
    inputStr = sys.stdin.readline()
    inputDict = json.loads(inputStr)
    extract_metadata(inputDict, sys.argv[1])

if __name__ == "__main__":
    main()
