# Copyright 2017-2018 Jungho Shin, Fawzi Mohamed
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import setup_paths
import json
import os.path, sys, subprocess
from nomadcore.local_meta_info import loadJsonFile, InfoKindEl
from nomadcore.parser_backend import JsonParseEventsWriterBackend
from nomadcore.parse_streamed_dicts import *
import logging

base_path = os.path.abspath(os.path.dirname(__file__))
with open(base_path+'/setting.json', 'r') as file_conf: repo_conf = json.load(file_conf)
repo_base_path = repo_conf['repo_base_path']
rawdata_archive_path = repo_conf['rawdata_archive_path']

def calculateTags(inputDict, backend, calcUri):

    repoSectUri = inputDict.get("section_repository_info.uri")
    if repoSectUri:
        backend.openContext(repoSectUri)
    else:
        backend.openContext(calcUri)
        repoSect = backend.openSection("section_repository_info")
    parserSect = backend.openSection("section_repository_parserdata")

    try:
        repo_dic_sub = subprocess.Popen([os.path.join(repo_base_path, 'bin/python'), base_path+'/extract_from_repo.py', rawdata_archive_path], stdin = subprocess.PIPE, stdout = subprocess.PIPE)
        inputDict_str = json.dumps(inputDict)+'\n'
        repo_dic_sub_out_all = repo_dic_sub.communicate(inputDict_str.encode('utf-8'))
        repo_dic_sub_out = repo_dic_sub_out_all[0]
        repo_dic = json.loads(repo_dic_sub_out.decode("utf-8"))
        backend.addValue("repository_checksum", repo_dic['checksum'])
        backend.addValue("repository_chemical_formula", repo_dic['formula'])
        backend.addValue("repository_parser_id", repo_dic['prog_name'] + ' v1.0')
        for el in repo_dic['elements']:
            backend.addValue("repository_atomic_elements", el)
        backend.addValue("repository_basis_set_type", repo_dic['ansatz'])
        backend.addValue("repository_code_version", repo_dic['prog'].split(repo_dic['prog_name'])[0])
        backend.addValue("repository_crystal_system", repo_dic['symmetry'])
        backend.addValue("repository_program_name", repo_dic['prog_name'])
        backend.addValue("repository_spacegroup_nr", repo_dic['ng'])
        backend.addValue("repository_system_type", repo_dic['periodicity'])
        for t in repo_dic['H_types']:
            backend.addValue("repository_xc_treatment", t)
    except:
        logging.exception("error evaluating async tags")

    backend.closeSection("section_repository_parserdata", parserSect)
    if repoSectUri:
        backend.closeContext(repoSectUri)
    else:
        backend.closeSection("section_repository_info", repoSect)
        backend.closeContext(calcUri)
    sys.stdout.flush()

def main():
    metapath = '../../../../nomad-meta-info/meta_info/nomad_meta_info/' +\
               'common.nomadmetainfo.json'
    metaInfoPath = os.path.normpath(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), metapath))

    metaInfoEnv, warns = loadJsonFile(filePath=metaInfoPath,
                                    dependencyLoader=None,
                                    extraArgsHandling=InfoKindEl.ADD_EXTRA_ARGS,
                                    uri=None)
    backend = JsonParseEventsWriterBackend(metaInfoEnv)
    calcUri = sys.argv[1]
    backend.startedParsingSession(
        calcUri,
        parserInfo = {'name':'RepoTagsNormalizer', 'version': '1.0'})

    dictReader=ParseStreamedDicts(sys.stdin)
    #dictReader=ParseStreamedDicts(open("/u/jungho/myscratch/nomad-lab-base/normalizers/repo-tags/test/examples/scalaOut1.txt", 'r'))

    while True:
        inputDict=dictReader.readNextDict()
        if inputDict is None:
            break
        calculateTags(inputDict, backend, calcUri)
    backend.finishedParsingSession("ParseSuccess", None)
    sys.stdout.flush()

if __name__ == "__main__":
    main()
