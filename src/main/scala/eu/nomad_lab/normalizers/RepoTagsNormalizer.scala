/*
 * Copyright 2017-2018 Jungho Shin, Fawzi Mohamed
 * 
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package eu.nomad_lab.normalizers

import eu.{ nomad_lab => lab }
import eu.nomad_lab.DefaultPythonInterpreter
import org.{ json4s => jn }
import scala.collection.breakOut
import eu.nomad_lab.normalize.ExternalNormalizerGenerator
import eu.nomad_lab.normalize.Normalizer
import eu.nomad_lab.meta
import eu.nomad_lab.ref.ObjectKind
import eu.nomad_lab.query
import eu.nomad_lab.resolve._
import eu.nomad_lab.h5.EmitJsonVisitor
import eu.nomad_lab.h5.H5EagerScanner
import eu.nomad_lab.h5.SectionH5
import eu.nomad_lab.parsers.ExternalParserWrapper
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.H5Lib
import scala.collection.mutable.StringBuilder
import util.control.NonFatal

object RepoTagsNormalizer extends ExternalNormalizerGenerator(
  name = "RepoTagsNormalizer",
  info = jn.JObject(
    ("name" -> jn.JString("RepoTagsNormalizer")) ::
      ("parserId" -> jn.JString("RepoTagsNormalizer" + lab.RepoTagsVersionInfo.version)) ::
      ("versionInfo" -> jn.JObject(
        ("nomadCoreVersion" -> jn.JObject(lab.NomadCoreVersionInfo.toMap.map {
          case (k, v) => k -> jn.JString(v.toString)
        }(breakOut): List[(String, jn.JString)])) ::
          (lab.RepoTagsVersionInfo.toMap.map {
            case (key, value) =>
              (key -> jn.JString(value.toString))
          }(breakOut): List[(String, jn.JString)])
      )) :: Nil
  ),
  context = "calculation_context",
  filter = query.CompiledQuery(query.QueryExpression("program_name and atom_positions"), meta.KnownMetaInfoEnvs.all),
  cmd = Seq(DefaultPythonInterpreter.pythonExe(), "${envDir}/normalizers/repo-tags/normalizer/normalizer-repo-tags/calculate_repo_tags.py",
    "${contextUri}", "${archivePath}"),
  resList = Seq(
    "normalizer-repo-tags/calculate_repo_tags.py",
    "normalizer-repo-tags/extract_from_repo.py",
    "normalizer-repo-tags/setup_paths.py",
    "normalizer-repo-tags/setting.json",
    "nomad_meta_info/public.nomadmetainfo.json",
    "nomad_meta_info/common.nomadmetainfo.json",
    "nomad_meta_info/meta_types.nomadmetainfo.json",
    "nomad_meta_info/stats.nomadmetainfo.json"
  ) ++ DefaultPythonInterpreter.commonFiles(),
  dirMap = Map(
    "normalizer-repo-tags" -> "normalizers/repo-tags/normalizer/normalizer-repo-tags",
    "nomad_meta_info" -> "nomad-meta-info/meta_info/nomad_meta_info",
    "python" -> "python-common/common/python/nomadcore"
  ) ++ DefaultPythonInterpreter.commonDirMapping(),
  metaInfoEnv = lab.meta.KnownMetaInfoEnvs.all
) {

  override def stdInHandler(context: ResolvedRef)(wrapper: ExternalParserWrapper)(pIn: java.io.OutputStream): Unit = {
    val trace = Normalizer.trace
    val out: java.io.Writer =
      new java.io.BufferedWriter(new java.io.OutputStreamWriter(pIn));
    val stringBuilder = if (trace)
      new StringBuilder
    else
      null
    def writeOut(s: String): Unit = {
      out.write(s)
      if (trace) stringBuilder ++= s
    }
    def flush(): Unit = {
      out.flush()
      if (trace) {
        logger.info(stringBuilder.result())
        stringBuilder.clear()
      }
    }
    writeOut("[")
    var isFirst = true
    try {
      context match {
        case Calculation(archiveSet, c) =>
          val programNames: Set[String] = (for (v <- c.valueTable(Seq("section_run", "program_name"))) yield (v.stringValue))(breakOut)
          val programVersions: Set[String] = (for (v <- c.valueTable(Seq("section_run", "program_version"))) yield (v.stringValue))(breakOut)
          val xcFunctionals: Set[String] = (for (v <- c.valueTable(Seq("section_run", "section_method", "section_xc_functionals", "xc_functional_name"))) yield (v.stringValue))(breakOut)
          val electronicStructureMethods: Set[String] = (for (v <- c.valueTable(Seq("section_run", "section_method", "electronic_structure_method"))) yield (v.stringValue))(breakOut)
          val singleConfSections = c.sectionTable(Seq("section_run", "section_single_configuration_calculation"))
          val nEnergyEvaluations = singleConfSections.lengthL
          var lastGeometry: Option[SectionH5] = None
          var lastEnergy: Option[Double] = None
          val cGroup = c.calculationGroup
          val mainFileUris: Seq[String] = try {
            val mainFileAttr: Long = H5Lib.attributeOpen(
              cGroup,
              attributeName = "mainFileUri", attributeType = "C"
            )
            val mainFiles: Array[String] = H5Lib.attributeReadStr(mainFileAttr)
            H5Lib.attributeClose(mainFileAttr)
            mainFiles.toSeq
          } catch {
            case NonFatal(e) =>
              Seq()
          }
          if (nEnergyEvaluations > 0) {
            val lastEnergyEval: SectionH5 = singleConfSections(nEnergyEvaluations - 1)
            lastGeometry = lastEnergyEval.maybeValue("single_configuration_calculation_to_system_ref").map(_.longValue) match {
              case Some(idx) =>
                val sysSections = c.sectionTable(Seq("section_run", "section_system"))
                Some(sysSections(idx))
              case None =>
                None
            }
            lastEnergy = lastEnergyEval.maybeValue("energy_total").map(_.doubleValue)
          }
          val repoInfo = c.sectionTable(Seq("section_repository_info"))
          writeOut(s"""{
  "program_name": ${JsonUtils.normalizedStr(jn.JArray(programNames.map(jn.JString(_))(breakOut)))},
  "program_version": ${JsonUtils.normalizedStr(jn.JArray(programVersions.map(jn.JString(_))(breakOut)))},
  "xc_functional_name": ${JsonUtils.normalizedStr(jn.JArray(xcFunctionals.map(jn.JString(_))(breakOut)))},
  "electronic_structure_method": ${JsonUtils.normalizedStr(jn.JArray(electronicStructureMethods.map(jn.JString(_))(breakOut)))},
  "section_single_confguration_calculation.length": ${nEnergyEvaluations},
  "energy_total.last": ${lastEnergy.getOrElse("null")},
  "main_file_uri": ${JsonUtils.normalizedStr(jn.JArray(mainFileUris.map(jn.JString(_))(breakOut)))},
  "section_system.last": """)
          lastGeometry match {
            case None => "null"
            case Some(geo) =>
              val visitor = new EmitJsonVisitor(
                writeOut = writeOut
              )
              val scanner = new H5EagerScanner
              scanner.scanResolvedRef(Section(archiveSet, geo), visitor)
          }
          writeOut(""",
  "section_repository_info.uri": """)
          if (repoInfo.isEmpty)
            writeOut("null")
          else
            writeOut(JsonUtils.escapeString(repoInfo(0).toRef.toUriStr(ObjectKind.NormalizedData)))
          writeOut("}]\n")
          flush()
        case r =>
          throw new Exception(s"RepoTagsNormalizer expected a calculation as context, but got $r")
      }
    } finally {
      out.close()
      pIn.close()
      wrapper.sendStatus = ExternalParserWrapper.SendStatus.Finished
    }
  }
}
